from html import unescape

import requests, re

def get_web_page_title(url):

    page = requests.get(url)
    match = re.search('<title>(.*?)</title>', page.text)
    title = match.group(1) if match else 'No title'
    return unescape(title)


