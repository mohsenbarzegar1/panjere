from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import JsonResponse
from django.core.paginator import Paginator
from django.views.decorators.csrf import csrf_exempt

from feed.models import Link
from feed.forms import LinkForm
from feed.constants import strings
from feed.telegram import send_new_link_to_admin

def home_page(request):
    form = LinkForm()
    if request.method == 'POST':
        form = LinkForm(data=request.POST)
        new_link = post_new_link(request ,form)
        if new_link:
            messages.success(request, strings.FORM_SUCCESS_SENT_FOR_CONFIRMATION_MESSAGE)
    links = Link.objects.all().filter(status='W')

    paginator = Paginator(links, 4)
    page_number = request.GET.get('page')
    feed_page = paginator.get_page(page_number)

    return render(
        request,
        'home.html',
        {
            'form': form,
            'feed_page': feed_page
        }
    )

@csrf_exempt
def submit_form(request):
    form = LinkForm()

    if request.method == 'POST':
        form = LinkForm(data=request.POST)
        new_link = post_new_link(request ,form)
        if new_link:
            messages.success(request, strings.FORM_SUCCESS_SENT_FOR_CONFIRMATION_MESSAGE)

    return render(
        request,
        'submit_form.html',
        {
            'form': form,
        }
    )

def set_status(request, status, id):
    link = Link.objects.get(id=id)
    if link.status != 'W':
        link.status = 'W'
        link.save()

    responseData = {
        'id': link.id,
        'status': link.status,
        'link' : link.text
    }
    return JsonResponse(responseData)

def post_new_link(request, form):
    if form.is_valid():
        new_link = form.save()

        current_server_url = request.build_absolute_uri('/')
        send_new_link_to_admin({
            'id': new_link.id,
            'url': new_link.text,
            'server_url': current_server_url
        })

        return new_link
    return None

