from django.test import TestCase
from django.utils.html import escape

from feed.models import Link
from feed.forms import LinkForm

from feed import utils
from feed.constants import tests_url_helpers as helper, strings


class HomePageTest(TestCase):

    def post_invalid_input(self):
        return self.client.post('/', data={'text': ''})

    def test_uses_home_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_can_save_a_POST_request(self):
        response = self.client.post('/', data=helper.VALID_POST_DATA)

        self.assertEqual(Link.objects.count(), 1)
        new_link = Link.objects.first()
        self.assertEqual(new_link.text, helper.EXAMPLE_LINK)

    def test_only_saves_items_when_necessary(self):
        self.client.get('/')
        self.assertEqual(Link.objects.count(), 0)

    def test_displays_success_message_on_post(self):
        response = self.client.post('/', data=helper.VALID_POST_DATA)

        self.assertIn(escape(strings.FORM_SUCCESS_SENT_FOR_CONFIRMATION_MESSAGE), response.content.decode())

        Link.objects.create(text=helper.EXAMPLE_LINK, status='W')
        Link.objects.create(text=helper.EXAMPLE_LINK.replace('.com', '.net'), status='W')

    def test_displays_all_list_items(self):
        response = self.client.get('/')

        self.assertIn(helper.EXAMPLE_LINK, response.content.decode())
        self.assertIn(helper.EXAMPLE_LINK.replace('.com', '.net'), response.content.decode())



    def test_displays_link_title(self):
        self.client.post('/', data=helper.VALID_POST_DATA)

        # submitted links default status is 'P'
        # and the home page only shows the link if it's status is 'W'
        Link.objects.filter(text=helper.EXAMPLE_LINK).update(status='W')

        response = self.client.get('/')

        link_title = utils.get_web_page_title(helper.EXAMPLE_LINK)

        self.assertIn(link_title, response.content.decode())

    def test_saving_entered_link_status_as_pending(self):
        self.client.post('/', data=helper.VALID_POST_DATA)

        saved_link = Link.objects.first()
        self.assertEqual(saved_link.status, 'P')

    def test_only_displays_approved_links(self):
        self.client.post('/', data=helper.VALID_POST_DATA)

        home_page_source = self.client.get('/')
        self.assertNotIn(helper.EXAMPLE_LINK, home_page_source.content.decode())

    def test_for_invalid_input_renders_home_template(self):
        response = self.post_invalid_input()
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

    def test_validation_errors_are_shown_on_home_page(self):
        response = self.post_invalid_input()
        self.assertContains(response, escape(strings.FORM_EMPTY_LINK_ERROR))

    def test_for_invalid_input_passes_form_on_home_page(self):
        response = self.post_invalid_input()
        self.assertIsInstance(response.context['form'], LinkForm)

    def test_invalid_links_arent_saved(self):
        response = self.post_invalid_input()
        self.assertEqual(Link.objects.count(), 0)

    def test_home_page_uses_link_form(self):
        response = self.client.get('/')
        self.assertIsInstance(response.context['form'], LinkForm)

    def test_displays_link_form(self):
        response = self.client.get('/')
        self.assertIsInstance(response.context['form'], LinkForm)
        self.assertContains(response, 'name="text"')

    def test_sets_link_status_to_well(self):
        self.client.post('/', data=helper.VALID_POST_DATA)
        new_link = Link.objects.first()

        response = self.client.get(f"/setstatus/W/{new_link.id}/")
        new_link.refresh_from_db()

        self.assertEqual(new_link.status, 'W')
