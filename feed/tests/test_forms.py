from django.test import TestCase

from feed.constants import tests_url_helpers as helper, strings

from feed.forms import LinkForm


class LinkFormTest(TestCase):

    def test_form_renders_link_text_input(self):
        form = LinkForm()
        self.assertIn(f'placeholder="{strings.FORM_INPUT_PALCE_HOLDER_TEXT}"', form.as_p())
        self.assertIn('class="form-control input-lg"', form.as_p())

    def test_form_validation_for_blank_links(self):
        form = LinkForm(data={'text': ''})
        self.assertFalse(form.is_valid())
        self.assertIn(strings.FORM_EMPTY_LINK_ERROR ,form.errors['text'])

    def test_form_save_handles_saving_to_a_list(self):
        form = LinkForm(data={
            'text': helper.EXAMPLE_LINK,
            **helper.VALID_CAPTCHA
        })
        new_link = form.save()

    def test_form_validation_for_unavailable_links(self):
        form = LinkForm(data={
            'text': helper.EXAMPLE_UAVAILABLE_LINK,
            **helper.VALID_CAPTCHA,
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors.get('text'), [strings.FORM_UNAVAILABLE_WEB_PAGE_ERROR])

    def test_form_validation_for_invalid_links(self):
        form = LinkForm(data={
            'text': helper.EXAMPLE_INVALID_LINK,
            **helper.VALID_CAPTCHA,
        })
        self.assertFalse(form.is_valid())

    def test_form_validation_for_non_existent_links(self):
        form = LinkForm(data={
            'text': helper.EXAMPLE_NON_EXISTENT_LINK,
            **helper.VALID_CAPTCHA,
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['text'], [strings.FORM_UNAVAILABLE_WEB_PAGE_ERROR])

    def test_form_validation_for_invalid_captcha(self):
        form = LinkForm(data={
            'text': helper.EXAMPLE_LINK,
            **helper.INVALID_CAPTCHA,
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['captcha'], [strings.FORM_INVALID_CAPTCHA_ERROR])

    def test_form_validation_for_empty_captcha(self):
        form = LinkForm(data={
            'text': helper.EXAMPLE_LINK,
            **helper.INVALID_EMPTY_CAPTCHA,
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['captcha'], [strings.FORM_EMPTY_CAPTCHA_ERROR])

    def test_form_validation_for_restricted_links(self):
        form = LinkForm(data={
            'text': 'http://jadi.net',
            **helper.VALID_CAPTCHA,
        })
        self.assertTrue(form.is_valid())
