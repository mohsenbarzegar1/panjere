EXAMPLE_LINK = "http://example.com"
EXAMPLE_INVALID_LINK = "http:/example.com"
EXAMPLE_NON_EXISTENT_LINK = "http://yekwebpagekevojoodnadarad.com"
EXAMPLE_UAVAILABLE_LINK= "http://example.com/foo.txt"
EXAMPLE_RESTRICTED_LINK= "http://jadi.net"

VALID_CAPTCHA = {
    'captcha_0': "abc",
    'captcha_1': "PASSED"
}

INVALID_CAPTCHA = {
    'captcha_0': "abc",
    'captcha_1': "INVALID"
}

INVALID_EMPTY_CAPTCHA = {
    'captcha_0': "abc",
    'captcha_1': ""
}

VALID_POST_DATA = {
    'text':EXAMPLE_LINK,
    **VALID_CAPTCHA,
}
