from django.utils.translation import gettext as _

FORM_EMPTY_LINK_ERROR = _("You can't have an empty link")
FORM_REQUIRED_LINK_ERROR = _("Please enter a url")
FORM_INVALID_URL_ERROR = _("Url format is invalid")
FORM_UNAVAILABLE_WEB_PAGE_ERROR = _("This web page is unavailable")

FORM_INVALID_CAPTCHA_ERROR = _("Invalid CAPTCHA")
FORM_EMPTY_CAPTCHA_ERROR = _("Please enter the CAPTCHA")

FORM_LINK_ALREADY_EXISTS_ERROR = _("This link is already sumbmited")

FORM_SUCCESS_SENT_FOR_CONFIRMATION_MESSAGE = _("Link was sent for admin's confirmation")
FORM_INPUT_PALCE_HOLDER_TEXT = _("Enter a link")

SITE_TITLE = _("Panjere")



