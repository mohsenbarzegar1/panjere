from django.contrib import admin
from django.http import HttpResponseRedirect

from feed.models import Link


@admin.register(Link)
class LinkAdmin(admin.ModelAdmin):
    list_display = ("text", "status", "has_published", "clickable_link")
    readonly_fields = ('manual_publish_url',)

    def response_change(self, request, obj):
        if "_publish_to_telegram" in request.POST:
            obj.publish_to_telegram()
            self.message_user(request, "Link has been published to telegram")
            return HttpResponseRedirect(".")
        return super().response_change(request, obj)
