from rest_framework import serializers
from feed.models import Link

class LinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Link
        fields = ("text", "title")