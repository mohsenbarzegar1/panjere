from django.test import TestCase
from django.utils.translation import gettext as _

from feed.models import Link

class RandomLinkFrameTest(TestCase):

    def setUp(self):

        args = {
            "status": "W",
            "has_published" : "True",
        }

        Link.objects.bulk_create([
            Link(text="http://gsm.ir", **args),
            Link(text="http://example.com", **args),
            Link(text="https://www.python.org", **args),
            Link(text="https://duckduckgo.com", **args),
            Link(text="http://github.com", **args),
        ])

    def test_uses_frame_template(self):
        response = self.client.get('/api/random/5/')
        self.assertTemplateUsed(response, 'frame.html')

    def test_frame_page_uses_correct_template(self):
        response = self.client.get('/frame/')
        self.assertContains(response, _('Frame'))
        self.assertTemplateUsed(response, 'panjere_frame.html')
