from django.utils.translation import gettext as _
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver

from feed.models import Link


class LinkFrameTest(StaticLiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()

        args = {
            "status": "W",
            "has_published" : "True",
        }

        Link.objects.bulk_create([
            Link(text="http://gsm.ir", **args),
            Link(text="http://example.com", **args),
            Link(text="https://www.python.org", **args),
            Link(text="https://duckduckgo.com", **args),
            Link(text="http://github.com", **args),
        ])

    def tearDown(self):
        self.browser.quit()

    def test_user_can_see_random_links_by_count(self):
        RANDOM_LINKS_COUNT = 5
        self.browser.get(self.live_server_url + f'/api/random/{RANDOM_LINKS_COUNT}/')

        assert 'Random Links' in self.browser.title

        links = self.browser.find_elements_by_tag_name("a")
        self.assertEqual(len(links), RANDOM_LINKS_COUNT)

    def test_user_can_see_random_links_in_iframe(self):
        self.browser.get(self.live_server_url + '/frame')

        assert _('Frame') in self.browser.title
