Django==3.1
django-simple-captcha==0.5.12
dnspython==2.0.0
requests==2.24.0
selenium==3.141.0
psycopg2
djangorestframework
django-cors-headers
